import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-left',
  templateUrl: './left.component.html',
  styleUrls: ['./left.component.css']
})
export class LeftComponent implements OnInit {
  userPermission: number;
  userInfo: any;
  constructor() { }

  ngOnInit() {
    this.userInfo = JSON.parse(localStorage.getItem('currentUser'));
    // this.userPermission =  this.userInfo.user_role;
  }

}
