import { Component, OnInit, NgZone } from '@angular/core';
import { DashboardService } from '../Services/dashboard.service';
import { NgxSpinnerService } from 'ngx-spinner';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  allUserList: any[];

  private chart: am4charts.XYChart;
  constructor(private dashService: DashboardService,
    private spinner: NgxSpinnerService, private zone: NgZone) { }

  ngOnInit() {
    am4core.useTheme(am4themes_animated);
    // Themes end

    // Create chart instance
    let chart = am4core.create("chartdiv", am4charts.XYChart);

    // Export
    chart.exporting.menu = new am4core.ExportMenu();

    // Data for both series
    var data = [{
      "year": "6m",
      "income": 32.5,
      "expenses": 30.1
    }, {
      "year": "12m",
      "income": 26.2,
      "expenses": 25.5
    }, {
      "year": "18m",
      "income": 60.1,
      "expenses": 34.9
    }, {
      "year": "24m",
      "income": 39.5,
      "expenses": 39.1
    }, {
      "year": "30m",
      "income": 30.6,
      "expenses": 18.2,
      "lineDash": "5,5",
    }, {
      "year": "36m",
      "income": 34.6,
      "expenses": 29.2,
      "lineDash": "6,5",
    }, {
      "year": "42m",
      "income": 51.6,
      "expenses": 25.2,

    }, {
      "year": "48m",
      "income": 44.6,
      "expenses": 32.2,

    }, {
      "year": "54m",
      "income": 34.6,
      "expenses": 35.2,

    }, {
      "year": "60m",
      "income": 64.6,
      "expenses": 20.2,

    }];


    /* Create axes */
    let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "year";
    categoryAxis.renderer.minGridDistance = 30;

    /* Create value axis */
    let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.title.text = "Users";
    /* Create series */
    let columnSeries = chart.series.push(new am4charts.ColumnSeries());
    columnSeries.name = "User Activity";
    columnSeries.dataFields.valueY = "income";
    columnSeries.dataFields.categoryX = "year";

    columnSeries.columns.template.tooltipText = "[#fff font-size: 15px]{name} in {categoryX}:\n[/][#fff font-size: 20px]{valueY}[/] [#fff]{additional}[/]"
    columnSeries.columns.template.propertyFields.fillOpacity = "fillOpacity";
    columnSeries.columns.template.propertyFields.stroke = "stroke";
    columnSeries.columns.template.propertyFields.strokeWidth = "strokeWidth";
    columnSeries.columns.template.propertyFields.strokeDasharray = "columnDash";
    columnSeries.tooltip.label.textAlign = "middle";

    let lineSeries = chart.series.push(new am4charts.LineSeries());
    lineSeries.name = "User Activity";
    lineSeries.dataFields.valueY = "expenses";
    lineSeries.dataFields.categoryX = "year";

    lineSeries.stroke = am4core.color("#5cf3bc");
    lineSeries.strokeWidth = 1;
    lineSeries.propertyFields.strokeDasharray = "lineDash";
    lineSeries.tooltip.label.textAlign = "middle";

    let bullet = lineSeries.bullets.push(new am4charts.Bullet());
    bullet.fill = am4core.color("#fdd400"); // tooltips grab fill from parent by default
    bullet.tooltipText = "[#fff font-size: 15px]{name} in {categoryX}:\n[/][#fff font-size: 20px]{valueY}[/] [#fff]{additional}[/]"
    let circle = bullet.createChild(am4core.Circle);
    circle.radius = 4;
    circle.fill = am4core.color("#fff");
    circle.strokeWidth = 3;
    let title = chart.titles.create();
    title.text = "Conversation Trend";
    title.fontSize = 10;
    title.marginBottom = 5;
    chart.data = data;
  }

  ngOnDestroy() {
    this.zone.runOutsideAngular(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });
  }

}
