import { Component, OnInit } from '@angular/core';
import { UserService } from '../Services/user.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.css']
})
export class UserAddComponent implements OnInit {

  userForm: { username: string, email: string, password: string, confirm_password: string, user_role: string };
  passwordMsg: string;
  showpasswordMsg: boolean;
  selectMsg: string;
  showSelectMsg: boolean;

  constructor(private userService: UserService,private router: Router,
    private spinner: NgxSpinnerService, private toastr: ToastrService) { }

  ngOnInit() {
    this.initialiseForm();
  }
  initialiseForm() {
    this.userForm = {
      username: '', 
      email: '', 
      password: '', 
      confirm_password: '',
      user_role: 'Select User Role'
    };
  }
  addUserData() {
    if (this.userForm.password === this.userForm.confirm_password && this.userForm.user_role !== 'Select User Role') {
      this.showpasswordMsg = false;
      this.showSelectMsg = false;
      this.spinner.show();
      this.userService.addUser(this.userForm).subscribe(res => {
        if (res) {
          this.spinner.hide();
          this.initialiseForm();
          this.toastr.success(res['msg']);
          this.router.navigate(['/user-list']);
        }
      }, err => {
        this.spinner.hide();
        this.toastr.error(err);
      })
    } else if(this.userForm.user_role === 'Select User Role'){
      this.showSelectMsg = true;
      this.selectMsg = 'Please select user role.'
    } else if(this.userForm.password !== this.userForm.confirm_password){
      this.showpasswordMsg = true;
      this.passwordMsg = 'Password and Confirm Password should be same.'
    }


  }
}
