import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { AuthenticationService } from '../Services/authorization.service';
import { ChatService } from '../Services/chat.service';
import { CommonService } from '../Services/common.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  userInfo: any;
  notificationData: {room: string, chat_user: string, noti_type: string};
  @Output() acceptUserToChat:EventEmitter<any>= new EventEmitter();
  showNotification: boolean;
  previousNotification: any[];
  readMsg: boolean;

  constructor(private authService: AuthenticationService,
    private router: Router, private spinner: NgxSpinnerService, 
    private toastr: ToastrService,private chatService: ChatService,
    private commonService: CommonService,) { }

  ngOnInit() {
    this.notificationData = {room: '', chat_user: '', noti_type: ''};
    this.previousNotification = [];
    this.userInfo = JSON.parse(localStorage.getItem('currentUser'));
    this.getNotification();
    this.getPreviousNotification();
    // localStorage.setItem('notiValue',JSON.stringify(this.notificationData));
  }

  getNotification(){
    this.chatService.getNotificationMessages().subscribe(message => {
      this.showNotification = true;
      this.notificationData = message;
      console.log('data',this.notificationData);
    });
  }
  getPreviousNotification() {
    this.spinner.show();
    this.commonService.getPreviousNotif(this.userInfo.username).subscribe((res: any[]) => {
      if (res) {
        this.spinner.hide();
        this.previousNotification = res;
        console.log(this.previousNotification);
      }
    }, err => {
      this.spinner.hide();
      this.toastr.error(err);
    })
  }

  // transfer
  transferOtherAgent(){

  }
  acceptAgent(){ //selectedUser
    this.router.navigate(['/chat']);
    // localStorage.setItem('notiValue',JSON.stringify(this.notificationData));
    this.acceptUserToChat.emit({room: this.notificationData.room, chat_user: this.notificationData.chat_user}); 
    this.commonService.changeMessage(this.notificationData);
    this.notificationData = {room: '', chat_user: '', noti_type: ''};
    console.log('eemiit');
    this.showNotification = false;
  }


  acceptInPrevious(rowData){
  const newObj = {id: rowData.id};
  this.spinner.show();
    this.commonService.updateReadMessage(newObj).subscribe(res => {
      if (res['success']) {
        this.readMsg = true;
        this.spinner.hide();
        this.previousNotification.map(x => {
          if(x.id === rowData.id){
            x.read = 1;
          }
        })
        console.log(this.previousNotification);
      }
    }, err => {
      this.spinner.hide();
      this.toastr.error(err);
    })
  }
transferOtherInPrevious(){

}





  logoutUser() {
    const newObj = {username: this.userInfo.username};
    this.spinner.show();
    this.authService.logout(newObj).subscribe(res => {
      this.spinner.hide();
      this.toastr.success(res['msg']);
      this.disconnectAgentChat();
      this.router.navigate(['/login']);
    })
  }

  disconnectAgentChat() {
    this.chatService.disconnectAgent(this.userInfo.username); 
  }
  // ngOnDestroy() {
  //   this.chatService.disconnectAgent(this.userInfo.username);
  // }
}
