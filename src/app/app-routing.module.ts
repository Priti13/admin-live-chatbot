import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { LeftComponent } from './left/left.component';
import { LoginComponent } from './login/login.component';
import { AuthguardService } from './guards/authguard.service';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ChatmenuComponent } from './chatmenu/chatmenu.component';
import { UserAddComponent } from './user-add/user-add.component';
import { UserListComponent } from './user-list/user-list.component';

const routes: Routes = [
    // { path: '', component: HeaderComponent },
  //  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthguardService] },
    { path: 'home', component: HomeComponent, canActivate: [AuthguardService] },
    { path: 'dashboard', component: DashboardComponent, canActivate: [AuthguardService] },
    { path: 'chat', component: ChatmenuComponent, canActivate: [AuthguardService] },
    { path: 'add-user', component: UserAddComponent, canActivate: [AuthguardService] },
    { path: 'user-list', component: UserListComponent, canActivate: [AuthguardService] },
    { path: 'login', component: LoginComponent },
    // { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: '', component: LoginComponent },
];


@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
