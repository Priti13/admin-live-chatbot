export class User {
    username: string;
    password: string;
}

export class CurrentUser {
    type: string;
    token: string;
    refresh: string;
    name: string;
    designation: string;
    department: string;
    email: string;
    sapId: string;
    id: number;
    permissions: any[];
}


