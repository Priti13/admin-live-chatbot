import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../Services/authorization.service';
import { User } from '../models/user';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: User = new User();
  msg = 'Login Successfully!';
  errormsg = 'Username & Password is not correct';
  agentInfo: any;
  constructor(private authService: AuthenticationService,
    private router: Router, private spinner: NgxSpinnerService,
     private toastr: ToastrService,) { }

  ngOnInit() {
    // TODO: already logged-in, so return dashboard
    if (this.authService.currentUserValue && this.authService.currentUserValue.status) {
      this.router.navigateByUrl("/dashboard")
    }
    if (this.authService.currentUserValue === null) {
      this.authService.cleanPreviousUser();
    }
  }

  login() {
    this.spinner.show(); 
    this.authService.userLogin(this.loginForm).subscribe(res => {
      this.spinner.hide();
      this.toastr.success(res['msg']);
        this.router.navigate(['/dashboard']);
      })
  }



}
