import { Component, OnInit, Input, SimpleChanges, Output, EventEmitter, AfterViewChecked, ViewChild, ElementRef } from '@angular/core';
import { ChatService } from '../Services/chat.service';
import { DashboardService } from '../Services/dashboard.service';
import { ToastrService } from 'ngx-toastr';
declare var $: any;
import * as moment from 'moment';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css'],
  // providers: [ChatService]
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChatComponent implements OnInit, AfterViewChecked {
  // sending message
  @Input() sessionId: any
  @Input() currentUser: any
  @Output() userExperience: EventEmitter<any> = new EventEmitter();
  @Output() disconnectBot: EventEmitter<any> = new EventEmitter();
  currentMsg: string;
  // coming msg
  messageData: any[] = [];
  agentInfo: any;
  historyMessage: any[];
  showSpinner: boolean;
  uniqueDateArr: any[];
  formatedHistoryMessage: any[];
  today: string;
  constructor(private chatService: ChatService,
    private dashService: DashboardService, private toastr: ToastrService) { }
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  ngOnInit() {
    this.formatedHistoryMessage = [];
    this.today = moment().format('YYYY-MM-DD');
  }
  // call history
  getHistoryMessage() {
    this.showSpinner = true;
    this.historyMessage = [];
    this.dashService.getHistoryData(this.sessionId, this.currentUser, this.agentInfo.username).subscribe((res: any[]) => {
      if (res) {
        this.showSpinner = false;
        this.historyMessage = res;
        this.sortData();
        this.findDate();
        // #scrollingchat
        // this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
        // this.scrollSet();
      }
    }, err => {
      this.showSpinner = false;
      this.toastr.error(err);
    })
  }
  ngAfterViewChecked() {
    try {
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    } catch (err) { }
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.agentInfo = JSON.parse(localStorage.getItem('currentUser'));
    console.log('value changed', this.sessionId, this.currentUser);
    this.messageData = [];
    this.chatService.getJoinedRoom(this.sessionId, this.currentUser, this.agentInfo.username);
    this.getHistoryMessage();
    this.getMessage();
    this.getUserExperience();
  }
  getUserExperience() {
    this.chatService.userExperienceValue().subscribe(message => {
      this.userExperience.emit(message);
    });
  }
  getMessage() {
    this.chatService.getMessages().subscribe(message => {
      // if(message.msgType == 'emotion'){
      //   this.userExperience.emit(message);
      // } else{
      message['section'] = 'leftSide';
      this.messageData.push(message);
      this.scrollSet();
      console.log(this.messageData);
      // }
    });
  }
  // send
  sendMessage(msg) {
    const newSendmsg = { msg: msg, 'section': 'rightSide' };
    this.messageData.push(newSendmsg);
    const newMsg = { 'msg': msg, 'room': this.sessionId };
    this.chatService.sendMessage(newMsg);
    this.currentMsg = '';
    this.scrollSet();
  }

  scrollSet() {
    $('#scrollingchat').stop().animate({ scrollTop: $('#scrollingchat')[0].scrollHeight }, 1000);
  }
  sortData() {
    return this.historyMessage.sort((a, b) => {
      return <any>new Date(a.time) - <any>new Date(b.time);
    });
  }
  getDifferentdate() {
    this.uniqueDateArr = [];
    this.today = this.today.slice(0, 10);
    console.log(this.today);
    for (let i = 0; i < this.historyMessage.length; i++) {
      let compareDate = this.historyMessage[i].time.slice(0, 10);
      if (this.uniqueDateArr.indexOf(compareDate) === -1) {
        this.uniqueDateArr.push(compareDate);
      }
    }
  }

  findDate(){
    this.getDifferentdate();
    let histmsgArry = [];
    for (let i = 0; i < this.uniqueDateArr.length; i++) {
      histmsgArry = [];
      for (let his = 0; his < this.historyMessage.length; his++) {
        let compareDate = this.historyMessage[his].time.slice(0, 10);
        if (this.uniqueDateArr[i] === compareDate ) {
          histmsgArry.push(this.historyMessage[his]);
        }
      }
      const newObj = {date: this.uniqueDateArr[i], histortMsg: histmsgArry};
      this.formatedHistoryMessage.push(newObj);
    }
  }

  disconnectSessionchat() {
    this.chatService.disconnectRoom(this.sessionId, this.agentInfo.username);
    this.disconnectBot.emit({ diconnected: 'diconnected' });
  }
  ngOnDestroy() {
    this.chatService.disconnectRoom(this.sessionId, this.agentInfo.username);
  }

}
