import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

const url = environment.url;
@Injectable({
  providedIn: 'root'
})

export class CommonService {
  private messageSource = new BehaviorSubject({ room: '', chat_user: '', noti_type: '' });
  currentMessage = this.messageSource.asObservable();
  constructor(private http: HttpClient) {
  }

  changeMessage(message) {
    this.messageSource.next(message)
  }
  // get organisation data
  getPreviousNotif(username): Observable<any> {
    return this.http.get(url + `admin/notifications?agent=${username}`)
  }
  // get organisation data
  updateReadMessage(ID): Observable<any> {
    const formData: FormData = new FormData();
    for (const key in ID) {
      if (ID.hasOwnProperty(key)) {
        formData.append(key, ID[key]);
      }
    }
    return this.http.post(url + `admin/notifications`, formData)
  }
}

