import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { throwError, Observable } from 'rxjs';

const url = environment.url;
@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http: HttpClient) { }

  getUserList(): Observable<any> {
    return this.http.get(url + `admin/chatbot_users`)
  }
  // get organisation data
  getOrganisationData(sessionID, currentUser, username): Observable<any> {
    return this.http.get(url + `admin/company_details?chat_user=${currentUser}&agent=${username}&room=${sessionID}`)
  }
    // get organisation data
    getTableData(): Observable<any> {
      return this.http.get(url + `admin/tickethandled`)
    }
  // get organisation data
  getHistoryData(sessionID, currentUser, username): Observable<any> {
    return this.http.get(url + `admin/agent_chat_history?chat_user=${currentUser}&agent=${username}&room=${sessionID}`)
  }
}

// sessionID, currentUser, username  chat_user=${currentUser}&agent=${username}&room=${sessionID}