import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import * as io from 'socket.io-client';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})

export class ChatService {

  public socket;
  constructor() {
    // const username = JSON.parse(localStorage.getItem('currentUser')).username;
    // this.socket = io(environment.url, {
    //   query: {agent_window: 1,agent: username}
    // });
  }
  // all socket at run time

  // public getConnection(username) {
  //   this.socket = io(environment.url, {
  //     query: {agent_window: 1,agent: username}
  //   });
  // }

  public getNotificationMessages() {
    return Observable.create((observer) => {
      this.socket.on('notification', (data) => {
        observer.next(data);
      });

      // this.socket.on('currentchatExp', (data) => {
      //   data['currentUserExp'] = 'currentUserExp';
      //   observer.next(data);
      // });
    });
  }

  sendNotificationTransfer(message) {
    this.socket.emit('text', message);
  }

  public disconnectAgent(username) {
    const newObj = {agent_window: 1,agent: username};
    this.socket.emit("disconnect", newObj);
    this.socket.disconnect();
  }
  

  // all chat socket
  public getJoinedRoom(sessionID, currentUser, username) {
    const joinObj = {
      room: sessionID,
      chat_user: currentUser,
      agent: username 
    }
    this.socket.emit('joined', joinObj);
  }

  public getMessages() {
    return Observable.create((observer) => {
      this.socket.on('message', (data) => {
        observer.next(data);
      });
      // this.socket.on('emotion', (data) => {
      //   data['msgType'] = 'emotion';
      //   observer.next(data);
      // });
    });
  }

  public userExperienceValue() {
    return Observable.create((observer) => {
      this.socket.on('emotion', (data) => {
        observer.next(data);
      });
    });
  }

  sendMessage(message) {
    this.socket.emit('text', message);
  }

  public disconnectRoom(sessionID,username) {
    const newObj = {room: sessionID,agent: username};
    this.socket.emit("leaved", newObj);
    // this.socket.disconnect();
  }
}

// this.socket = io(environment.url, {
//   query: {
//     agent_window: 1,
//     room: sessionID,
//     chat_user: currentUser,
//     agent: username
//   }
// });








// getMessages() {
//   const observable = new Observable(observer => {
//     this.socket.on('message', (data) => {
//       observer.next(data);
//     });

//     return () => {
//       this.socket.emit("windowclose", "");
//     };
//   });
//   return observable;
// }
