import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ServereventService {

  constructor() { }

  getEevntSource(url: string): EventSource {
    return new EventSource(url);
  }
  getServerSentEvent() {
    return Observable.create(observer => {
      const eventSource = this.getEevntSource(environment.url);

      eventSource.onmessage = event => {
        console.log(event.data);
        observer.next(event);
      }
      eventSource.onerror = error => {
        console.log(error);
        observer.error(error);
      }
    })
  }
}
