import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { throwError, Observable } from 'rxjs';

const url = environment.url;
@Injectable({
  providedIn: 'root'
})
export class AgentService {

  constructor(private http: HttpClient) { }

   addUser(userForm): Observable<any> {
    const formData: FormData = new FormData();
    for (const key in userForm) {
      if (userForm.hasOwnProperty(key)) {
        formData.append(key, userForm[key]);
      }
    }
  return this.http.post(url + `addusers`, formData)
}
  getUserRoleList(): Observable<any> {
  return this.http.get(url + `agentlist`)
  }
}
