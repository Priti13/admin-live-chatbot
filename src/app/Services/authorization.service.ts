import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { environment } from './../../environments/environment';
import { User } from '../models/user';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;

  constructor(private http: HttpClient,private spinner: NgxSpinnerService,
    private toastr: ToastrService) {

    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue() {
    return this.currentUserSubject.value;
  }
  public removeCurrentUser(): boolean {
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(JSON.parse(localStorage.getItem('currentUser')));
    return true;
  }
  userLogin(obj: User): Observable<any> {
    const formData: FormData = new FormData();
    for (const key in obj) {
      if (obj.hasOwnProperty(key)) {
        formData.append(key, obj[key]);
      }
    }
    const url = environment.url + 'admin/login';
    return this.http.post<any>(url, formData)
      .pipe(catchError(this.errorHandler),
        map(userData => {
          if (userData['success']) {
            let tempUser: any = {
               'status':  userData['success'],
              'token': userData.data.token ? userData.data.token : undefined,
              'user_role': userData.data.user_role,
              'username': userData.data.username
            }
            localStorage.setItem('currentUser', JSON.stringify(tempUser));
            this.currentUserSubject.next(tempUser);
          } else {
            this.toastr.error(userData['msg']);
          }
          return userData;
        }));
  }


  logout(obj): Observable<any> {
    const url = environment.url + 'admin/logout';
    const formData: FormData = new FormData();
    for (const key in obj) {
      if (obj.hasOwnProperty(key)) {
        formData.append(key, obj[key]);
      }
    }
    return this.http.post<any>(url, formData)
      .pipe(catchError(this.errorHandler),
        tap(resData => {
          if (resData['success'] === true) {
            this.cleanPreviousUser()
          }
        })
      )
  }

  cleanPreviousUser() {
    localStorage.clear();
    this.currentUserSubject.next(null);
  }
  
  errorHandler(respError: HttpErrorResponse) {
    this.spinner.hide();
    if (respError.error instanceof ErrorEvent) {
      this.toastr.error('Client Side Error: ' + respError);
    } else {
      this.toastr.error('Server Side Error: ' + respError)
    }
    return throwError(respError || 'Server Downgrade Error');
  }

}
