import { Component, OnInit, Output, EventEmitter, Input, AfterViewInit, ViewChild } from '@angular/core';
import { DashboardService } from '../Services/dashboard.service';
import { ChatService } from '../Services/chat.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { HeaderComponent } from "../header/header.component";
import { CommonService } from '../Services/common.service';

@Component({
  selector: 'app-chatmenu',
  templateUrl: './chatmenu.component.html',
  styleUrls: ['./chatmenu.component.css'],
})
export class ChatmenuComponent implements OnInit, AfterViewInit {
  allUserList: any[];
  showChatPanel: boolean = false;
  // sending message
  sessionId: string;
  currentUser: string;
  orgData: any;
  currentuserexp: any;
  tableData: any[];
  selectedrowData: {
    "closed": string,
    "description": string,
    "inc": string,
    "resolved": string,
    "sid": string,
    "start": string,
    "subject": string
  };

  //header
  notificationDataHeader: { room: string, chat_user: string, noti_type: string };

  @ViewChild(HeaderComponent) child;
  customerRating: any[];
  ratingArr: string[];

  constructor(private dashService: DashboardService, private commonService: CommonService,
    private spinner: NgxSpinnerService, private toastr: ToastrService) { }

  ngOnInit() {
    this.intialiseProperty();
    this.userListData();
    this.openChatFromNotification();
  }

  intialiseProperty() {
    this.allUserList = [];
    this.currentUser = '';
    this.sessionId = '';
    this.currentuserexp = '';
    this.tableData = [];
    this.customerRating = [];
    this.selectedrowData = {
      "closed": "",
      "description": "",
      "inc": "",
      "resolved": "",
      "sid": "",
      "start": "",
      "subject": ""
    };

  }
  openChatFromNotification() {
    this.commonService.currentMessage.subscribe(notifi => {
      this.notificationDataHeader = notifi;
      if (this.notificationDataHeader.room !== '') {
        this.allUserList.map(x => {
          if (x.username !== this.notificationDataHeader.chat_user) {
            this.allUserList.push({ session_id: this.notificationDataHeader.room, username: this.notificationDataHeader.chat_user });
          }
        })
        this.startChatSession(this.notificationDataHeader.room, this.notificationDataHeader.chat_user);
      }
    })
  }
  ngAfterViewInit() {
  }
  // get all user list data
  userListData() {
    this.spinner.show();
    this.dashService.getUserList().subscribe((res: any[]) => {
      if (res) {
        this.spinner.hide();
        this.allUserList = res['data'];
      }
    }, err => {
      this.spinner.hide();
      this.toastr.error(err);
    })
  }

  // start connection
  startChatSession(sessionId, user) {
    this.showChatPanel = true;
    this.currentUser = user;
    this.sessionId = sessionId;
    this.organisationData(sessionId, user);
  }
  // get current user experience 
  getCurrentUserExp(event) {
    this.currentuserexp = event.currentuserexp;
    console.log('Satisfied', event);
  }
  getDisconnection(event) {
    if (event.diconnected === 'diconnected') {
      this.showChatPanel = false;
    }
  }
  // get all user list data (methos of third panel)
  organisationData(sessionId, user) {
    this.spinner.show();
    const agentInfo = JSON.parse(localStorage.getItem('currentUser'));
    this.organisationTableData();
    this.dashService.getOrganisationData(sessionId, user, agentInfo.username).subscribe(res => {
      if (res) {
        this.spinner.hide();
        this.orgData = res['data'];
        this.getStar(this.orgData.rating);
      }
    }, err => {
      this.spinner.hide();
      this.toastr.error(err);
    })
  }
  // get all user list data (methos of third panel)
  organisationTableData() {
    this.spinner.show();
    this.dashService.getTableData().subscribe((res: any[]) => {
      if (res) {
        this.spinner.hide();
        this.tableData = res['data'];
      }
    }, err => {
      this.spinner.hide();
      this.toastr.error(err);
    })
  }
  // get rating star
  getStar(rating) {
    const outputCMVal = Math.round(rating * 10) / 10;
    this.ratingArr = outputCMVal.toString().split('.');
    this.customerRating.length = Number(this.ratingArr[0]);
    for (let i = 0; i < Number(this.ratingArr[0]); i++) {
      this.customerRating[i] = i + 1;
    }
  }
  // selecet row and display data
  selectedRow(data) {
    this.selectedrowData = data;
  }

  // from notification 
  notiSelectedUser(event) {
    if (this.currentUser === event.chat_user) {
    } else {
      this.showChatPanel = false;
      this.allUserList.map(x => {
        if (x.username !== event.chat_user) {
          this.allUserList.push({ session_id: event.room, username: event.chat_user });
        }
      })
      this.startChatSession(event.room, event.chat_user);
    }
  }

}
 // this.allUserList.splice(0, 0, { session_id: notiData.room, username: notiData.chat_user });
// this.allUserList.splice(0, 0, { session_id: event.room, username: event.chat_user });
