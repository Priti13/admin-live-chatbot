import { Injectable } from '@angular/core';
import { CanActivate, Router, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { AuthenticationService } from './../Services/authorization.service';

@Injectable({
  providedIn: 'root'
})
export class AuthguardService implements CanActivate {
  constructor(private router: Router, private authenticationService: AuthenticationService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    // var currentUser = localStorage.getItem('currentUser');
    const currentUser = this.authenticationService.currentUserValue;
   // if (currentUser && currentUser.token != undefined) {
    //  if (currentUser && currentUser.status) {
      // authorised so return true
      return true;
    // }
    // not logged in so redirect to login page with the return url
    // this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
    // return false;
  }
}
