import { Component, OnInit } from '@angular/core';
import { UserService } from '../Services/user.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  userRoleList: any[];

  constructor(private userService: UserService,
    private spinner: NgxSpinnerService, private toastr: ToastrService) { }

  ngOnInit() {
    this.getUserRole();
  }
  getUserRole(){
    this.spinner.show();
    this.userService.getUserRoleList().subscribe((res: any[]) => {
      if (res) {
        this.userRoleList = res['data'];
        this.spinner.hide();
      }
    }, err => {
      this.spinner.hide();
      this.toastr.error(err);
    })
  }
}
